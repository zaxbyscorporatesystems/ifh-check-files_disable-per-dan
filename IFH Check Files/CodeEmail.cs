﻿using System;
using System.Net;
using System.Net.Mail;
using System.Text;

namespace IFH_Check_Files
{
    class CodeEmail : IDisposable
    {
        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
        public string Emessage
        { set; get; }
        /// <summary>Send the email message.</summary>
        /// <returns>True if sent, false if sror encountered.</returns>
        public bool SendEmail()
        {
            bool rslt = true;
            try
            {
                System.Net.Mail.MailMessage message = new System.Net.Mail.MailMessage();
                message.Subject = "Weekly PFG File and Missing Invoice Audit";
                StringBuilder sbMessage = new StringBuilder();
                System.Net.Mail.SmtpClient smtp = new System.Net.Mail.SmtpClient("email.zaxbys.com");
                StringBuilder sbRecEmail = new StringBuilder();
                //message.To.Add("dcarter@zaxbys.com");
                message.To.Add("pjohnson@zaxbys.com");
                message.From = new System.Net.Mail.MailAddress("ablashaw@zaxbys.com", "PFG Files");
                
                message.CC.Add("ayoungblood@zaxbys.com");
                message.CC.Add("ablashaw@zaxbys.com");
                //sbMessage.Append("-----TEST-TEST-TEST-TEST-TEST-----");
                //sbMessage.Append(Environment.NewLine);
                //sbMessage.Append(Environment.NewLine);
                //sbMessage.Append("-----DO NOT LOAD-----");
                //sbMessage.Append(Environment.NewLine);
                //sbMessage.Append(Environment.NewLine);
              //  sbMessage.Append("The files below do not appear to have been loaded from PFG-IFH.");
                sbMessage.Append(string.Format($@"{Emessage}"));
                sbMessage.Append(Environment.NewLine);
                sbMessage.Append(Environment.NewLine);
                message.Priority = MailPriority.High;
                message.BodyEncoding = System.Text.Encoding.GetEncoding("utf-8");
                message.Body = sbMessage.ToString();
                smtp.Credentials = CredentialCache.DefaultNetworkCredentials;
                smtp.UseDefaultCredentials = false;
                smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                smtp.EnableSsl = false;
                smtp.Send(message);

                smtp.Dispose();
                message.Dispose();
            }
            catch (Exception ex)
            {
                rslt = false;
            }
            return rslt;
        }

    }
}
