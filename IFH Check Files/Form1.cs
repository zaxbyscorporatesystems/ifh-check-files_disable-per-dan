﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WinSCP;
using System.IO;

namespace IFH_Check_Files
{
    public partial class Form1 : Form
    {
        string _LocalDirectory = @"c:\data\IFH\",     //Local directory where the files will be downloaded
            loadedFyle=string.Empty,
_logFyle = string.Empty;

        DateTime dteLookFor1 = DateTime.Now;//.AddDays(-1);
        string currentdate = string.Empty;
       // string currenttime = string.Empty;

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            // _logFyle = _LocalDirectory + "//PFG Load.log";
            currentdate = dteLookFor1.ToString("yyyy") + dteLookFor1.ToString("MM") + dteLookFor1.ToString("dd");
            //currenttime = dteLookFor1.ToString("hh") + dteLookFor1.ToString("mm") + dteLookFor1.ToString("ss");

            // IFH import
            string  fileName = string.Empty,          //File name, which one will be downloaded
                message=string.Empty;
            //_ftpURL = "ftp3.pfgc.com",     //"ftp://ftp3.pfgc.com",             //Host URL or address of the FTP server
            //    _UserName = "zaxby00",                 //User Name of the FTP server
            //    _Password = "zaX$qGQ#",              //Password of the FTP server
            //    _ftpDirectory = "outbound",    
            DateTime dteLookFor = DateTime.Now;     //.AddDays(-1);
            StringBuilder sbSql = new StringBuilder();
            object oFound = null;
                                                    // File.AppendAllText(_logFyle, DateTime.Now + "---Process start" + Environment.NewLine);
            fileName = "IFH_" + currentdate + "*.txt";             //File name, which one will be downloaded
            try
            {
                // Setup session options
                //SessionOptions sessionOptions = new SessionOptions
                //{
                //    Protocol = Protocol.Ftp,        // .Sftp,
                //    HostName = _ftpURL,
                //    UserName = _UserName,
                //    Password = _Password
                //};
                //lstbxFound.Items.Clear();
                //using (Session session = new Session())
                //{
                    // Connect
                    //session.Open(sessionOptions);

                    // Download files
                    //TransferOptions transferOptions = new TransferOptions();
                    //transferOptions.TransferMode = TransferMode.Binary;

                    //TransferOperationResult transferResult;
                    for (int step = 0; step < 1; step++)
                    {
                        dteLookFor1 = DateTime.Now.AddDays(-(step+1));
                        currentdate = dteLookFor1.ToString("yyyy") + dteLookFor1.ToString("MM") + dteLookFor1.ToString("dd");
                        string fyleDteSearch = "*" + currentdate + "*.txt";             //File name, which one will be downloaded

                        string[] fylesPFG = Directory.GetFiles($@"{@"\\goober\ftpusers\external\VendorInvoices\PFG\Loaded"}", $@"{ fyleDteSearch}");


                        foreach (string fyleName in fylesPFG)
                        {
                            loadedFyle = fyleName.Replace(@"\\goober\ftpusers\external\VendorInvoices\PFG\Loaded\", "");
                            sbSql.Clear();
                            sbSql.Append("SELECT COUNT([import_file_name]) FROM[dbo].[ftp_files]");
                            sbSql.Append($@" WHERE import_file_name = '{loadedFyle}'");
                            using (CodeSQL clsSql = new CodeSQL())
                            {
                                oFound = clsSql.CmdScalar(Properties.Settings.Default.aConBob, sbSql.ToString());
                            }
                            if (oFound.ToString().Trim() != "1")
                            {
                                // lstbxFound.Items.Add($@"{loadedFyle} was not found in the FTP loaded table.");
                                message += $@"{loadedFyle} was not found in the FTP loaded table." + Environment.NewLine;
                            }
                        }

                    //fileName = "IFH_" + currentdate + "*.txt";             //File name, which one will be downloaded

                    //transferResult = session.GetFiles($@"/{_ftpDirectory}/{fileName}", $@"{_LocalDirectory}{fileName}", false, transferOptions);

                    //// Throw on any error
                    //transferResult.Check();

                    // Print results
                    //foreach (TransferEventArgs transfer in transferResult.Transfers)
                    //{
                    //    loadedFyle = transfer.FileName.Replace(@"/outbound/", "");
                    //    sbSql.Clear();
                    //    sbSql.Append("SELECT COUNT([import_file_name]) FROM[dbo].[ftp_files]");
                    //    sbSql.Append($@" WHERE import_file_name = '{loadedFyle}'");
                    //    using (CodeSQL clsSql = new CodeSQL())
                    //    {
                    //        oFound = clsSql.CmdScalar(Properties.Settings.Default.aConBob, sbSql.ToString());
                    //    }
                    //    if (oFound.ToString().Trim() != "1")
                    //    {
                    //        // lstbxFound.Items.Add($@"{loadedFyle} was not found in the FTP loaded table.");
                    //        message += $@"{loadedFyle} was not found in the FTP loaded table."+Environment.NewLine;
                    //    }
                    //}
                }
                // check invoices in  ftp_tmp but not in apinvhdr
                sbSql.Clear();
                sbSql.Append("SELECT X.*");
                sbSql.Append(" FROM");
                sbSql.Append(" (SELECT T.import_file_id, F.import_file_name, D.Distrib_Code, ");
                sbSql.Append(" CASE WHEN D.FileFormatID IN(1, 2, 3) THEN UPPER(RTRIM(LTRIM(SUBSTRING(T.field1, 2, 5)))) ELSE '' END AS vend_supplier_id,");
                sbSql.Append(" CASE WHEN D.FileFormatID = 1 THEN LTRIM(RTRIM(CONVERT(VARCHAR(10), CAST(SUBSTRING(T.field1, 7, 10) AS INT))))");
                sbSql.Append(" WHEN D.FileFormatID = 2 THEN(CASE WHEN ISNUMERIC(SUBSTRING(T.field1, 7, 10)) = 1 THEN CAST(CAST(SUBSTRING(T.field1, 7, 10) AS BIGINT) AS VARCHAR(10)) ELSE CAST(LTRIM(RTRIM(SUBSTRING(T.field1, 7, 10))) AS VARCHAR(10)) END)");
                sbSql.Append(" ELSE CAST(LTRIM(RTRIM(SUBSTRING(T.field1, 7, 20))) AS VARCHAR(20))  END AS customer_num,");
                sbSql.Append(" CASE WHEN D.FileFormatID = 1 THEN RTRIM(SUBSTRING(T.field1, 51, 7))");
                sbSql.Append(" WHEN D.FileFormatID = 2 THEN(CASE WHEN ISNUMERIC(SUBSTRING(T.field1, 51, 7)) = 1 THEN CAST(CAST(SUBSTRING(T.field1, 51, 7) AS BIGINT) AS VARCHAR(20)) ELSE CAST(LTRIM(RTRIM(SUBSTRING(T.field1, 51, 7))) AS VARCHAR(20)) END)");
                sbSql.Append(" WHEN D.FileFormatID = 3 THEN(CASE WHEN ISNUMERIC(SUBSTRING(T.field1, 51, 20)) = 1 THEN CAST(CAST(SUBSTRING(T.field1, 51, 20) AS DECIMAL(20, 0)) AS VARCHAR(20))");
                sbSql.Append(" ELSE CAST(LTRIM(RTRIM(SUBSTRING(T.field1, 51, 20))) AS VARCHAR(20)) END) END AS invoice_num");
                sbSql.Append(" FROM ftp_tmp T");
                sbSql.Append(" INNER JOIN ftp_files F ON F.import_file_id = T.import_file_id");
                sbSql.Append(" INNER JOIN SCM_Distributors D ON D.Distrib_ID = D.ParentDistrib_ID AND D.Distrib_Code = F.import_source");
                sbSql.Append(" WHERE LEFT(T.field1, 1) = 'H') AS X");
                sbSql.Append(" WHERE NOT EXISTS(SELECT 1 FROM apinvhdr H WHERE H.vend_supplier_id = X.vend_supplier_id AND H.customer_num = X.customer_num AND H.invoice_num = X.invoice_num)");
                sbSql.Append(" ORDER BY 1,4,5,6");
                DataSet dsMissing = new DataSet();
                using (CodeSQL clsSql = new CodeSQL())
                {
                    dsMissing = clsSql.CmdDataset(Properties.Settings.Default.aConBob, sbSql.ToString());
                }
                if (dsMissing.Tables.Count > 0 && dsMissing.Tables[0].Rows.Count > 0)
                {
                    message += Environment.NewLine;
                    message += "----------------------------------------------------" + Environment.NewLine;
                    message += "Invoices in ftp_tmp table but not in apinvhdr table." + Environment.NewLine;
                    message += "import_file_id\timport_file_name\tdistrib_Code\tvend_supplier_id\tcustomer_num\tinvoice_num" + Environment.NewLine;
                    foreach (DataRow dRow in dsMissing.Tables[0].Rows)
                    {
                        message += $@"{dRow["import_file_id"]}"+"\t"+$@"{dRow["import_file_name"]}" + "\t" + $@"{dRow["distrib_Code"]}" + "\t" + $@"{dRow["vend_supplier_id"]}" + "\t" + $@"{dRow["customer_num"]}" + "\t" + $@"{dRow["invoice_num"]}" + Environment.NewLine;
                    }
                }

                if (message.Trim().Length > 0)
                    {
                        using (CodeEmail clsEmail = new CodeEmail())
                        {
                            clsEmail.Emessage ="The files below do not appear to have been loaded from PFG."+Environment.NewLine+message;
                            clsEmail.SendEmail();
                        }
                    }
                    //else
                    //{
                    //    using (CodeEmail clsEmail = new CodeEmail())
                    //    {
                    //        clsEmail.Emessage = "All PFG files appear to have been loaded for the prior week." + Environment.NewLine;
                    //        clsEmail.SendEmail();
                    //    }
                    //}

                //}
            }
            catch (Exception ex)
            {
                //                Console.WriteLine("Error: {0}", ex.Message);
            }
            Application.Exit();
        }

    }
}

