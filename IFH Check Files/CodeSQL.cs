﻿using System;
using System.Data.SqlClient;
using System.Data;

namespace IFH_Check_Files
{
    class CodeSQL : IDisposable
    {
        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }

        public object CmdScalar(string con, string sqlCmd)
        {
            object rslt = 0;
            try
            {
                using (SqlConnection sqlCon = new SqlConnection(con))
                {
                    sqlCon.Open();
                    using (SqlCommand sqlQ = new SqlCommand())
                    {
                        sqlQ.Connection = sqlCon;
                        sqlQ.CommandText = sqlCmd;
                        rslt = sqlQ.ExecuteScalar();
                    }
                }
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
            }
            return rslt;
        }
        public DataTable CmdDataTable(string con, string sqlCmd)
        {
            DataSet dsRslt = new DataSet();
            try
            {
                using (SqlConnection sqlCon = new SqlConnection(con))
                {
                    sqlCon.Open();
                    using (SqlDataAdapter sqlDap = new SqlDataAdapter())
                    {
                        sqlDap.SelectCommand = new SqlCommand(sqlCmd, sqlCon);
                        sqlDap.Fill(dsRslt);
                    }
                }
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
            }
            return dsRslt.Tables[0];
        }
        public DataSet CmdDataset(string con, string sqlCmd)
        {
            DataSet dsRslt = new DataSet();
            try
            {
                using (SqlConnection sqlCon = new SqlConnection(con))
                {
                    sqlCon.Open();
                    using (SqlDataAdapter sqlDap = new SqlDataAdapter())
                    {
                        sqlDap.SelectCommand = new SqlCommand(sqlCmd, sqlCon);
                        sqlDap.Fill(dsRslt);
                    }
                }
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
            }
            return dsRslt;
        }

        public int CmdNonQuery(string con, string sqlCmd)
        {
            int rslt = 0;
            try
            {
                using (SqlConnection sqlCon = new SqlConnection(con))
                {
                    sqlCon.Open();
                    using (SqlCommand sqlQ = new SqlCommand())
                    {
                        sqlQ.Connection = sqlCon;
                        sqlQ.CommandText = sqlCmd;
                        rslt = sqlQ.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
            }
            return rslt;
        }
    }
}
